package com.devcamp.bookauthorapiv2.controllers;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.bookauthorapiv2.Author;
import com.devcamp.bookauthorapiv2.Book;

@RestController
@RequestMapping("/")
@CrossOrigin
public class BookControllerV2 {
    @GetMapping("/books")
    public ArrayList<String> booksApi(){
        //task 4
        Author author1 = new Author("Victor Hugo", "victor@yahoo.com", 'm');
        Author author2 = new Author("Marie Joy", "marie@gmail.com", 'f');
        Author author3 = new Author("Linh Lai", "linh@gmail.com", 'f');
        Author author4 = new Author("Micheal Korrs", "micheal@yahoo.com", 'm');
        Author author5 = new Author("Angelina Jolly", "jolly@gmail.com", 'f');
        Author author6 = new Author("Phoebe Wang", "phoebe@hotmail.com", 'f');
        System.out.println(author1);
        System.out.println(author2);
        System.out.println(author3);
        System.out.println(author4);
        System.out.println(author5);
        System.out.println(author6);
        //task 5
        ArrayList<Author> authorlist1 = new ArrayList<>();
        authorlist1.add(author1);
        authorlist1.add(author2);
        ArrayList<Author> authorlist2 = new ArrayList<>();
        authorlist2.add(author3);
        authorlist2.add(author4);
        ArrayList<Author> authorlist3 = new ArrayList<>();
        authorlist3.add(author5);
        authorlist3.add(author6);
        //task 6
        Book book1 = new Book("How to be good", authorlist1, 120000, 5);
        Book book2 = new Book("Beauty and the beast", authorlist2, 80000, 2);
        Book book3 = new Book("Calligraphy and life", authorlist3, 90000, 3);
        System.out.println(book1);
        System.out.println(book2);
        System.out.println(book3);
        //task 7
        ArrayList<String> bookList = new ArrayList<>();
        bookList.add(book1.toString());
        bookList.add(book2.toString());
        bookList.add(book3.toString());

        return bookList;
    }
}
